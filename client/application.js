import * as queries from './queries';

const SELECTORS = {
    FLIGHTS: '.js-flights-button',
    RESULTS: '.js-results-wrapper'
};

export default class Application {
    constructor(client) {
        this.client = client;
        this.wrapper = document.querySelector(SELECTORS.RESULTS);
        this.buttons = {
            flights: document.querySelector(SELECTORS.FLIGHTS)
        };

        this.buttons.flights.addEventListener('click', () => this.handleFlights());
    }

    async handleFlights() {
        let result = await this.client.query({ query: queries.flights });

        this.appendContentToWrapper(this.buildList(result.data.flights).trim());
    }

    appendContentToWrapper(content) {
        this.wrapper.innerHTML = '';
        this.wrapper.innerHTML = content;
    }

    buildList(list) {
        return `
        <ul>
            ${ list.map(item => {
                return `<li>${ Object.keys(item).map(field => {
                    return field.indexOf('__') === -1 ? `${field}: ${item[field]}` : '';
                }).join('<br>') }</li>`
            }).join('') }
        </ul>
        `;
    }
}
