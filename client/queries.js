import { gql } from 'apollo-boost';

export const flights = gql`
    query getFlights {
        flights {
            id
            name
        }
    }
`;
